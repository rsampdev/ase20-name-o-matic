const LOADER_HTML = `<svg class="animate-spin ml-1 h-5 w-5 inline-block text-black" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24">
          <circle class="opacity-25" cx="12" cy="12" r="10" stroke="currentColor" stroke-width="4"></circle>
          <path class="opacity-75" fill="currentColor" d="M4 12a8 8 0 018-8V0C5.373 0 0 5.373 0 12h4zm2 5.291A7.962 7.962 0 014 12H0c0 3.042 1.135 5.824 3 7.938l3-2.647z"></path>
        </svg>`;

const getNamesFromTextarea = (textarea) => {
  return textarea.value
    .split("\n")
    .map((x) => x.trim())
    .filter(Boolean);
};

const showLoader = (el) => {
  el.innerHTML = LOADER_HTML;
};

const showName = (el, name) => {
  el.textContent = name;
};

function shuffle(a) {
  var j, x, i;

  for (i = a.length - 1; i > 0; i--) {
    j = Math.floor(Math.random() * (i + 1));
    x = a[i];
    a[i] = a[j];
    a[j] = x;
  }

  return a;
}

const getNextName = (names, index) => {
  return names[index];
}

const main = () => {
  const elName = document.getElementById("js-name");
  const elButton = document.getElementById("js-button");
  const elTextarea = document.getElementById("js-textarea");

  let rawNames = getNamesFromTextarea(elTextarea);
  let names = shuffle(rawNames);
  let index = 0;

  let isLoading = false;

  const onButtonClick = () => {
    if (isLoading) {
      return;
    }
  
    isLoading = true;
    showLoader(elName);

    setTimeout(() => {
      rawNames = getNamesFromTextarea(elTextarea);

      if([...names].sort().toString() !== [...rawNames].sort().toString()) {
        names = shuffle(rawNames);
        index = 0;
      }
      
      showName(elName, getNextName(names, index));

      index === names.length - 1 ? index = 0 : index++;

      isLoading = false;
    }, 400);
  };

  elButton.addEventListener("click", onButtonClick);
};

main();